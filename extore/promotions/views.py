# -*- coding: utf-8 -*-

"""
Кастомные и дополнительные вьюхи

Created: 05.03.14 0:31
Author: Ivan Soshnikov, e-mail: ivan@wtp.su
"""

from oscar.apps.promotions.views import HomeView as CoreHomeView


class HomeView(CoreHomeView):
    """
    Главная страница магазина
    """

    template_name = 'promotions/my_homeview.html'